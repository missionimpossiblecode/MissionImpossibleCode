#Best explanation of why this coded is helpful: https://missionimpossiblecode.io/post/solution-for-reverse-engineering-linux-config-deltas-via-system-wide-diffing/

#Run this directly from this location with: curl https://gitlab.com/missionimpossiblecode/MissionImpossibleCode/-/raw/master/ConfigsnapCreateKnownGood.sh -o /tmp/ConfigsnapCreateKnownGood.sh ; sudo bash /tmp/ConfigsnapCreateKnownGood.sh

#Zerofootprint for both known good and compare-to systems - just delete /tmp/configsnap

if [[ -z "$(command -v python)" ]]; then 
  echo "Python must be installed and working, exiting..."
  echo "If you cannot install python on the systems, read here about building it in an isolated folder: https://stackoverflow.com/a/42903156"
  exit 5
fi
mkdir -p /tmp/configsnap
curl https://raw.githubusercontent.com/rackerlabs/configsnap/master/configsnap -o /tmp/configsnap/configsnap
chmod +x /tmp/configsnap/configsnap

cat > /tmp/configsnap/additional.conf <<'EOF_CONFIG'
[allmachineconfig]
Type: directory
Directory: /etc/

[userconfigs]
Type: directory
Directory: /home
File_Pattern: \..*

[systemduserservices]
Type: directory
Directory: /lib/systemd/user
File_Pattern: .*\.service$

[systemdsystemservices]
Type: directory
Directory: /lib/systemd/system
File_Pattern: .*\.service$
EOF_CONFIG

sudo /tmp/configsnap/configsnap --basedir=/tmp/configsnap/snaps --verbose --config=/tmp/configsnap/additional.conf --tag=crossmachinecompare --phase=knowngoodconfig

cat <<- EndOfMessage

Next Steps:

1. Sample scp command to pull this on a system to compare to: 
   sudo scp -r user_on_this_reference_system@referencesystemdnsorip:/tmp/configsnap /tmp/configsnap
2. Sample auto-compare command on compare-to system:
   sudo /tmp/configsnap/configsnap --basedir=/tmp/configsnap/snaps --verbose --config=/tmp/configsnap/additional.conf --tag=crossmachinecompare --pre=knowngoodconfig --phase=post

To use as a known good snapshot managed in a centralized location, copy "/tmp/configsnap" to a shared location (or use git to commit to a repository) where you can pull it onto any system you wish to test for drift or changes.

To clean the zero footprint install from any systems, run "sudo rm -rf /tmp/configsnap"

EndOfMessage
